import requests
import json

url = 'http://127.0.0.1:5000/api/v1/resultado/?alumno=LucasMontoya'


def get(url):
    r = requests.get(url)
    #print('Codigo de estado: {0}'.format(r.status_code))
    #print('Cabezera: {0}'.format(r.headers))
    json_content = json.loads(r.content)
    print(json_content)


def post():
    r = requests.post('https://httpbin.org/post', data = {'key':'value'})
    print('Status code: {0} Header {1}'.format(r.status_code, r.headers))
    json_content = json.loads(r.content)
    print(json_content)


def main(url):
    g = input('Ingrese metodo a usar: ')
    if g.lower() == 'get':
        get(url)
    elif g.lower() == 'post':
        post()
    else:
        print('Metodo No existe :')
        main(url)


if '__main__' == __name__:
    main(url)













