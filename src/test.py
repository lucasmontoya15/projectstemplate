from datetime import date
from .models import Person, initialize_database


def test_db():
    initialize_database()
    uncle_bob = Person(first_name='Bob', last_name='Smith', birthday=date(1960, 1, 15))
    uncle_bob.save()

    grandma = Person.create(first_name='Grandma', last_name='Williams', birthday=date(1935, 3, 1))

    print(grandma)

if __name__ == '__main__':
    test_db()
