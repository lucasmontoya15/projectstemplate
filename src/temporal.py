def make_fibonacci(limit, verbose=False):
    a = 0
    b = 1
    result = []
    while a < limit:
        if verbose:
            print(a, end=' , ')
        result.append(a)
        a, b = b, a + b
    if verbose:
        print()
    return result


print(make_fibonacci(200))


def muchos_items(separador, *args):
    print(separador.join(args))


muchos_items('.', 'Primero', 'Segundo', 'Tercero')



def muchos_items_palabras(**kwargs):
    for key in kwargs.keys():
        print('{0}: {1}'.format(key, kwargs[key]))


muchos_items_palabras(nombre='lucas', apellido='montoya', edad='25')



